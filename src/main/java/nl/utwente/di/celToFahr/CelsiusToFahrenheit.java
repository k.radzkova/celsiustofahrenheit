package nl.utwente.di.celToFahr;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Example of a Servlet that gets an Celsius and returns the Fahrenheit
 */

public class CelsiusToFahrenheit extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Translator translator;
	
    public void init() throws ServletException {
    	translator = new Translator();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Celsius To Fahrenheit Translator";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Degrees in Celsius: " +
                   request.getParameter("cel") + "\n" +
                "  <P>Converted to Fahrenheit: " +
                   Double.toString(translator.getFahrenheit(Double.parseDouble(request.getParameter("cel")))) +
                "</BODY></HTML>");
  }
  

}
