package nl.utwente.di.celToFahr;

public class Translator {

    public double getFahrenheit(double s) {
        return s*1.8+32;
    }
}
